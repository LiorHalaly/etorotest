﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Configuration;

namespace SampleProject
{
    public static class WebDriverFactory
    {
        public static IWebDriver Create<T>(params object[] args) where T : IWebDriver
        {
            // get type
            var type = typeof(T);

            // with params
            if (args == null || args.Length == 0)
            {
                return Activator.CreateInstance<T>();
            }

            // default constructor
            return (T)Activator.CreateInstance(type, args);
        }

        public static IWebDriver CreateNewDriver(string browserName)
        {
            string driverPath = ConfigurationManager.AppSettings["WebDriversPath"];

            if (browserName == "Chrome")
            {
                return Create<ChromeDriver>(driverPath);
            }
            else
            {
                var options = new FirefoxOptions { PageLoadStrategy = PageLoadStrategy.Normal };
                return Create<FirefoxDriver> (driverPath, options);
            }
        }
    }
}
