﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gravity.Drivers.Selenium;
using log4net;
using OpenQA.Selenium;

namespace SampleProject
{
    public class TrendsPage : PageBase
    {
        private static readonly ILog logger = LogManager
                  .GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public TrendsPage(IWebDriver webDriver) : base(webDriver) { }

        public TrendsPage Search(string keyword)
        {
            WebDriver.GetDisplayedElement(Repository.SEARCH_BOX).SendKeys(keyword);
            logger.Info($"send keys {keyword} to {Repository.SEARCH_BOX} text box");
            return this;
        }

        public TrendResultsPage AutoComplete(int index)
        {
            WebDriver.GetDisplayedElements(Repository.AUTO_COMPLETE).ElementAt(index).Click();
            logger.Info($"click on autocomplete first element");
            return new TrendResultsPage(WebDriver);
        }
    }
}
