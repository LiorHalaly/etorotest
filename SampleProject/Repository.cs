﻿using OpenQA.Selenium;

namespace SampleProject
{
    public static class Repository
    {
        public static readonly By SEARCH_BOX = By.XPath("//input[@type='search' and @ng-model='$mdAutocompleteCtrl.scope.searchText']");
        public static readonly By SEARCH_BOX_COUNTRY = By.XPath("(//div[@ng-click='focus()'])[1]");
        public static readonly By AUTO_COMPLETE = By.XPath("//ul[@class='md-autocomplete-suggestions']/li");
        public static readonly By SUB_CATEGORIES = By.XPath("(//div[@class='fe-atoms-generic-content-container' and ./div[@class='item']])[1]/div[@class='item']");
        public static readonly By TOPICS = By.XPath("(//div[@class='fe-atoms-generic-content-container' and .//div[@class='item']])[2]/div[@class='item']");
        public static readonly By SUB_NAMES = By.XPath(".//span[@ng-bind='bidiText']");
        public static readonly By SUB_CATEGORIES_NEXT = By.XPath("(//div[@class='fe-atoms-generic-content-container' and ./div[@class='item']])[1]//pagination/div/button[@ng-click='ctrl.updateToNextPage()']");
        public static readonly By TOPICS_NEXT = By.XPath("(//div[@class='fe-atoms-generic-content-container' and .//div[@class='item']])[2]//pagination/div/button[@ng-click='ctrl.updateToNextPage()']");
        public static readonly By RELEATED_FILTER = By.XPath("(//md-select[@ng-model='ctrl.viewField'])[2]");

        public static By CategoryByName(string name)
        {
            const string format =
                "(//div[@class='fe-atoms-generic-content-container' and ./div[@class='item']])[1]" +
                "//div[@class='item']" +
                "//span[@ng-bind and contains(.,'{0}')]";
            var locator = string.Format(format, name);
            return By.XPath(locator);
        }

        public static By RelatedFilterType(string filterType)
        {
            const string format = "//md-option[@class='md-ink-ripple' and @aria-selected='false' ]//div[contains(.,'{0}')]";
            var locator = string.Format(format, filterType);
            return By.XPath(locator);
        }
    }
}
