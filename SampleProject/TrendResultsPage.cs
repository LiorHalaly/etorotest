﻿using Gravity.Drivers.Selenium;
using log4net;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SampleProject
{
    public class TrendResultsPage : PageBase
    {
        public TrendResultsPage(IWebDriver webDriver) : base(webDriver) { }

        public override ILog Logger => LogManager.GetLogger(GetType().FullName);

        public TrendResultsPage Country(string country)
        {
            WebDriver.GetDisplayedElement(Repository.SEARCH_BOX_COUNTRY).Click();
            Logger.Info("click on country text box");

            WebDriver.GetDisplayedElements(Repository.SEARCH_BOX).ElementAt(1).SendKeys(country);
            Logger.Info($"send keys {country} to {Repository.SEARCH_BOX}");

            WebDriver.GetDisplayedElement(Repository.AUTO_COMPLETE).Click();
            Logger.Info($"click on {Repository.AUTO_COMPLETE} auto-complete");

            // Add for Firefox
            if (WebDriver.GetType().Name.IndexOf("FIREFOX", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                WebDriver.Navigate().Refresh();
            }
            Thread.Sleep(3000); // can be replaced with smart waiter      

            return this;
        }

        public TrendResultsPage SubCategory(string category)
        {
            // exit conditions
            var exitCompliance = false;

            // cycle categories
            do
            {
                exitCompliance = SubCategoryPulse(category);
            } while (!exitCompliance);

            // return all results
            return this;
        }

        private bool SubCategoryPulse(string category)
        {
            var subCategory = GetSubCategories().FirstOrDefault(e => e.Text.IndexOf(category, StringComparison.OrdinalIgnoreCase) >= 0);

            // exit conditions
            if (subCategory != null)
            {
                subCategory.Click();
                WebDriver.WaitForElementsToBeInvisible(Repository.SUB_CATEGORIES);
                WebDriver.WaitForDisplayedElement(Repository.SUB_CATEGORIES);

                //Thread.Sleep(3000); // ui refresh, can be used with waiter - stale/un-stale on panel
                return true;
            }

            // refresh next button
            WebDriver.WaitForEnabledElement(Repository.SUB_CATEGORIES_NEXT).SmartClick();
            Logger.Info($"click on next button {Repository.SUB_CATEGORIES_NEXT}");
            Thread.Sleep(1000); // can be replaced with smart waiter (stale & un-stale)

            // check exit compliance
            return !WebDriver.GetDisplayedElement(Repository.SUB_CATEGORIES_NEXT).Enabled;
        }

        private IEnumerable<IWebElement> GetSubCategories() => WebDriver
            .GetDisplayedElements(Repository.SUB_CATEGORIES).Select(e => e.FindElement(Repository.SUB_NAMES));

        public IEnumerable<string> GetRelatedTopics() => WebDriver
              .GetDisplayedElements(Repository.TOPICS)
              .Select(e => e.FindElement(Repository.SUB_NAMES).Text);

        public TrendResultsPage FilterReleatedTopics(string filterType)
        {
            // bypass scroll bug
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight);");

            WebDriver.GetDisplayedElement(Repository.RELEATED_FILTER).Click();
            Logger.Info($"click on rising button in {Repository.RELEATED_FILTER}");

            Thread.Sleep(2000); // can be replaced with waiter
            WebDriver.GetDisplayedElement(Repository.RelatedFilterType(filterType)).Click();
            Logger.Info($"click on top button in {filterType}");

            return this;
        }

        // can be generified into one method (with SubCategory)
        public IEnumerable<string> GetTopicsDescrition()
        {
            var allTopics = new List<string>();

            // exit conditions
            var exitCompliance = false;

            // cycle categories
            do
            {
                allTopics.AddRange(GetRelatedTopics());

                // refresh next button
                WebDriver.WaitForEnabledElement(Repository.SUB_CATEGORIES_NEXT).SmartClick();
                Logger.Info($"click on next button {Repository.SUB_CATEGORIES_NEXT}");
                Thread.Sleep(1000); // can be replaced with smart waiter (stale & un-stale)

                // check exit compliance
                exitCompliance = !WebDriver.GetDisplayedElement(Repository.SUB_CATEGORIES_NEXT).Enabled;
            } while (!exitCompliance);

            // last chunk
            allTopics.AddRange(GetRelatedTopics());

            // refresh page
            WebDriver.Navigate().Refresh();

            // return all results
            return allTopics;
        }
    }
}