using Gravity.Drivers.Selenium;
using NUnit.Framework;
using OpenQA.Selenium;
using SampleProject;
using System;
using System.Linq;

namespace Tests
{
    [TestFixture("Firefox")]
    [TestFixture("Chrome")]
    [Parallelizable(ParallelScope.Fixtures)]
    public class Tests
    {

        public Tests(string BrowserName)
        {
            driver = WebDriverFactory.CreateNewDriver(BrowserName);
        }

        private IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver.NavigateToUrl("https://trends.google.com");
        }

        [TearDown]
        public void Cleanup() => driver?.Dispose();
        [Test]
        public void Test1()
        {
            // initialize trend page
            var page = new TrendsPage(driver);

            // execute search and get sub category. throws exception if sub-category cannot be found
            var actual = page
                .Search("selenium")
                .AutoComplete(0)
                .Country("Israel")
                .SubCategory("tel aviv")
                .FilterReleatedTopics("Top")
                .GetTopicsDescrition();

            // assert
            const string expected = "Selenium - Software";
            var message = $"{expected} - Software not present";
            Assert.IsTrue(actual.Any(c => c.IndexOf(expected, StringComparison.OrdinalIgnoreCase) >= 0), message);
        }
    }
}