﻿using log4net;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SampleProject
{
    public class PageBase
    {
        public virtual ILog Logger  => LogManager.GetLogger(GetType().FullName);

        protected PageBase(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        public IWebDriver WebDriver { get; }
    }
}
